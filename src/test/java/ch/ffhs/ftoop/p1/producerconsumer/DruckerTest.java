package ch.ffhs.ftoop.p1.producerconsumer;

import org.junit.Test;
import student.TestCase;

public class DruckerTest extends TestCase {
	
	// Ich halte es für sinnvoller die eher das Zusammenspiel der Klassen zu
	// testen anstatt jede Klasse einzeln
	
	public void testPrintsNumbersOutOfSpeicher() throws InterruptedException {
		Speicher s1 = new Speicher();
		s1.setWert(25);
		Thread d1 = new Drucker(s1);
		d1.start();
		d1.sleep(2000);
		assertFuzzyEquals("25 ", systemOut().getHistory());
	}
}
