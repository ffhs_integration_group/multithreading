package ch.ffhs.ftoop.p1.producerconsumer;

import java.util.LinkedList;

import junit.framework.TestCase;

public class SpeicherTest extends TestCase {
	
		public void testIsHatWert() throws InterruptedException {
			Speicher s1 = new Speicher();
			s1.setWert(24);
			assertTrue(s1.isHatWert());
		}
		
		public void SetWert() throws InterruptedException {
			Speicher s1 = new Speicher();
			s1.setWert(24);
		}
		public void testGetWert() throws InterruptedException {
			Speicher s1 = new Speicher();
			s1.setWert(24);
			assertEquals(s1.getWert(), 24);
		}
}
