package ch.ffhs.ftoop.p1.producerconsumer;

import org.junit.Test;
import student.TestCase;

public class ZaehlerTest extends TestCase {
	
	public void testZaehlerSavesValueInSpeicher() throws InterruptedException {
		Speicher s1 = new Speicher();
		Zaehler z1 = new Zaehler(s1, 2, 10);
		z1.start();
		z1.sleep(2000);
		assertEquals(s1.getWert(), 10);
	}
}
